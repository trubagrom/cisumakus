**akus**

Хайдапов Дмитрий, P3222

Артемьев Никита, P3222

Люкшин Тимофей, P3222

Набиулин Иван, P3222


akus - способ поиска единомышленников, готовых образовать звуковую/музыкальную группу любого представления. 
Приложение работает с музыкантами, способными проявлять свои умения в группе, с людьми, распротраняющими своё творчество. 

## Функции:
- Регистрация своего музыкального профиля и создание карточки творца
- Возможность вступить в созданную группу в виде участника, беря на себя запрашиваемые функции 
- Возможность стать организатором группы и, указав необходимые слоты участников, отыскать их
- Общение с зарегестрированными пользователями в стоковом чате и обмен готовыми семплами/идеями
*выбор группы реализован в виде карточной системы и свайпов. Смах влево - отказ. Нажатие на слот с ролью потенциального участника - предложение содействия
*отбор участников организатор видит на странице администрирования группами. 


## Архитектура:

Веб-приложение, включающее в себя spring boot и JDBC для хранения данных. 
возможны отступления/корректировки 

структура бд также будет расписана на странице миро проекта(см. ссылку*)




## Аналоги:
- Vampr  https://www.vampr.me
-> неэффективен для создания целостной группы - преднозначен для поиска одного единомышленника, остальные цели - затруднительны
-> удобная кард-система, которой также инспирирован akus

- Soundcloud  https://soundcloud.com
-> Подходит для поиска начиннающих музыкантов и групп, но не выстраивает быструю связь между возможными партнёрами. Ставит акцент на стриминге. 

- BandFriend http://www.bandfriendapp.com
-> Представляет собой скорее социальную сеть с музыкальной тематикой 
-> Значительное углубление в функциональность малонужного мессенджера 

- JMB  https://www.joinmyband.co.uk
-> Обособляет музыкантов из разных городов друг от друга
-> Не систематизированная доска объявлений

- Join A Band https://www.join-a-band.com
-> наиболее схожий по идее, но разнящийся с akus'ом по реализации:
-> заранее зафиксированные немногочисленные роли творцов, привязанные к инструментам
->  больше подходит для единичной связи двух творцов



*примерная структура приложения с идеей оформления: https://miro.com/app/board/o9J_lsI5bGk=/

