package com.example.cisumakus.repos;
import com.example.cisumakus.domain.User;
import org.springframework.data.repository.CrudRepository;



public interface UserRepo extends CrudRepository<User, Long> {


}
